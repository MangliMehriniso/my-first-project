pragma solidity ^0.4.24;

contract Hello{
	function hello() returns (string){
		return "still another hello world example";
	}
	function anotherhello() returns(string){
		return "hello";
	}
}
